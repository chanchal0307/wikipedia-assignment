﻿Features:

	Python 3.5
	Latest Django 2.0.6
	SQLite for Database
	Beautiful Soup library for Scrapping the data

Project Setup:

	Create and activate virtual environment
	pip install -r requirements.txt
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver

Configured URLs:

	localhost:8000/admin
	Username: admin
	password: admin12345
	Description: URL for django admin panel. Enter localhost:8000/admin on 	browser and click on olympicsapp email.

    localhost:8000/save
	Description: URL to call the scrap_data function from scrapy.py and load the data into SQLite database.

	localhost:8000/data
	Description: URL to display the scrapped data for end users.
	Users can sort the data accordingly by clicking on the column heading. For example if a user wants to sort the data by bronze medals he can click on bronze column.

	localhost:8000/graph
	Description: URL to display the graph.


Note: Refer to Screenshot folder for output.



	





