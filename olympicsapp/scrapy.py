"""script to scrap data from given wikipedia url"""

import requests,bs4

url="https://en.wikipedia.org/wiki/2018_Winter_Olympics_medal_table"
r = requests.get(url)
soup = bs4.BeautifulSoup(r.text)
table = soup.find("table", {"class":"wikitable sortable plainrowheaders"})
rows = table.find_all('tr')
rows.pop(0)
rows.pop(len(rows)-1)
data,title=[],[]

def scrap_data():
    for counter in rows:
        td= counter.find_all('td')
        if td[0].has_attr("rowspan"):
            temp=int(td[0]["rowspan"])
            for i in range (temp):
                    data.append((int(td[0].text),int(td[1].text),int(td[2].text),int(td[3].text),int(td[3].text)))
        elif len(td)==5:
            data.append((int(td[0].text),int(td[1].text), int(td[2].text), int(td[3].text),int(td[4].text)))
        count = counter.find_all('img')
        for d in count:
            title.append((d["title"], d["src"]))
    return data,title








