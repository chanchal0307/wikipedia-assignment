""" Creates a table named Olympic in SQLite database"""
from django.db import models

class Olympic(models.Model):
    rank= models.IntegerField()
    gold=models.IntegerField()
    silver=models.IntegerField()
    bronze=models.IntegerField()
    total=models.IntegerField()
    title = models.CharField(max_length=250)
    flag= models.ImageField(upload_to='documents/')