from django.contrib import admin
from . models import Olympic
class OlympicAdmin(admin.ModelAdmin):
    list_display = ('rank','title','gold','silver','bronze','total')

admin.site.register(Olympic,OlympicAdmin)