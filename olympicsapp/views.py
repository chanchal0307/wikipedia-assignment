from django.http import HttpResponse
from django.shortcuts import render,render_to_response
from django.views.generic import ListView
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import mpld3 as mp
from .scrapy import scrap_data
from django.views import View
from .models import Olympic

class SaveDataView(View):
    """ Save scrapped data into database"""
    def get(self, request):
        data,title=scrap_data()
        for i in zip(data,title):
          olympic_data=Olympic(rank=i[0][0],gold=i[0][1],silver=i[0][2],bronze=i[0][3],total=i[0][4],title=i[1][0],flag=i[1][1])
          olympic_data.save()
          return HttpResponse("Data saved successfully into database ")

class DisplayData(ListView):
    """Generic view to dispaly list of data"""
    model = Olympic
    queryset = Olympic.objects.order_by('rank')

class ShowGraphView(View):
    """View to generate graph"""
    def get(self, request):
        df = pd.DataFrame(list(Olympic.objects.values_list('rank','gold','silver','bronze','title','total')),columns=['rank','gold','silver','bronze','title','total'])
        top_ten=(df.head(10))
        my_xticks = top_ten['title']
        fig = plt.figure()
        plt.xticks(top_ten['rank'],my_xticks)
        plt.plot(top_ten['gold'],color="yellow")
        plt.plot(top_ten['silver'],color="silver")
        plt.plot(top_ten['bronze'],color="chocolate")
        mp.save_html(fig, "/home/consultadd/Documents/MyDocuments/Assignments/moodys_analytics/Olympicsproject/olympicsapp/templates/olympicsapp/display.html")
        return render(request,"olympicsapp/display.html")





