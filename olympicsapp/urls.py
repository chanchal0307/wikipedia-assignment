""" Configures app urls"""

from django.urls import path
from .views import  SaveDataView,DisplayData,ShowGraphView

urlpatterns = [
path('save/', SaveDataView.as_view()),
path('data/', DisplayData.as_view()),
path('graph/',ShowGraphView.as_view()),
    ]