from django.apps import AppConfig


class OlympicsappConfig(AppConfig):
    name = 'olympicsapp'
